db.fruits.insertMany(
    [
        {
          name: "Apple",
          supplier: "Red Farms Inc.",
          stocks: 20,
          price: 40,
          onSale: true
        },

        {
          name: "Banana",
          supplier: "Yellow Farms",
          stocks: 15,
          price: 20,
          onSale: true
        },

        {
          name: "Kiwi",
          supplier: "Green Farming and Canning",
          stocks: 25,
          price: 50,
          onSale: true
        },
        {
          name: "Mango",
          supplier: "Yellow Farms",
          stocks: 10,
          price: 60,
          onSale: true
        },
        {
          name: "Dragon Fruit",
          supplier: "Red Farms Inc.",
          stocks: 10,
          price: 60,
          onSale: true
        }
    ]

)

db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}}
    //una muna onsale:true lahat, grinupo sila per supplier tapos sinuma ang total stocks nung per supplier
])

/*Aggregation Pipeline Stages

Aggregation is typically done i 2-3 steps, each process is called a stage*/





db.fruits.aggregate([

	//looked for and got all fruits that are onSale
	{$match:{onSale:true}},
	{$group: {_id:"$supplier",totalStocks:{$sum:"stocks"}}}
//pag naggroup parang ngawa temp documents

	])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:null,totalStocks:{$sum:"$stocks"}}}
	])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"AllOnSaleFruits",totalStocks:{$sum:"$stocks"}}}

	])

db.fruits.aggregate([

	{$match: {supplier:"Red Farms Inc."}},
	{$group: {_id:"RedFarmsInc",totalStocks:{$sum:"$stocks"}}}

	])
    
    db.fruits.aggregate([

	{$match: {$and:[{supplier:"Yellow Farms"},{onSale:true}]}},
	{$group: {_id:"YellowFarms",totalStocks:{$sum:"$stocks"}}}

	])

//$avg - is an operator used in $group stage

//$avg gets the avg of the numerical values of the indictaed field in grouped documents
//avg of all stocks of onsale na per supplier



db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id:"$supplier", avgStock: {$avg:"$stocks"}}}
	])

db.fruits.aggregate([
        {$match: {onSale:true}},
        {$group: {_id:null,avgPrice:{$avg:"$price"}}}
    ])
         
//null isang group

//$max will allow us to get the highest value out of all the values in a given field per group

db.fruits.aggregate([
	
	{$match:{onSale:true}},
	{$group:{_id:"highestStockOnSale", maxStock: {$max:"stocks"}}}
	])


///highest price for all items on sale
db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:null,maxPrice: {$max: "$price"}}}	
])

//$min gets the lowest value of the values in a given field per group
//get the lwest num of stock for all items on sale
db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group: {_id:"lowestStockOnSale", minStock:{$min:"$stocks"}}}
])

db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group: {_id:"lowestPriceOnSale", minPrice:{$min:"$price"}}}
])

db.fruits.aggregate([
	{$match: {price:{$lt:50}}},
	{$group: {_id:"minStockLess50", minStock:{$min:"$stocks"}}}
])

db.fruits.aggregate([
	{$match: {price:{$lt:50}}},
	{$group: {_id:"minStockLess50", minStock:{$min:"$stocks"}}}
])

//Other Stages

//$count - is a stage added after $match to count all items that matches our criteria

db.fruits.aggregate([
		{$match: {onSale:true}},
		{$count: "itemsOnSale"}
	])

db.fruits.aggregate([
		{$count: "allFruits"}
	])

db.fruits.aggregate([
	{$match: {price:{$lt:50}}},
	{$count: "itemPriceLessThan50"}
	])

//number of items with stocks less than 20

db.fruits.aggregate([
	{$match: {stocks:{$lt:20}}},
	{$count: "forRestock"}
	])

//$out - save/ouput the results in a new collection, however, this will overwrite a collection if it already exists
db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}},
	{$out: "stocksPerSupplier"}
	])
//new collection yey
db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group: {_id:"$supplier",avgPrice:{$avg:"$price"}}},
	{$out: "stocksPerSupplier"}
	])