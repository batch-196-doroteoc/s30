//aggregate

db.fruits.aggregate([
  {$match: {$and:[{supplier:"Yellow Farms"},{price:{$lt:50}}]}},
  {$count: "itemsInYellowFarmsPriceLessThan50"}
  ])

//aggregate

db.fruits.aggregate([
  {$match: {price:{$lt:30}}},
  {$count: "itemsPriceLessThan30"}
  ])

//aggregate

db.fruits.aggregate([
        {$match: {supplier:"Yellow Farms"}},
        {$group: {_id:"ItemsAvgPriceInYellowFarm", itemsAvgPrice:{$avg:"$price"}}}
    ])

//aggregate

db.fruits.aggregate([

  {$match: {supplier:"Red Farms Inc."}},
  {$group: {_id:"ItemsHighestPriceInRedFarmsInc",maxPrice: {$max: "$price"}}} 
])


//aggregate

db.fruits.aggregate([

  {$match: {supplier:"Red Farms Inc."}},
  {$group: {_id:"ItemsLowestPriceInRedFarmsInc",maxPrice: {$min: "$price"}}} 
])
